import 'package:flutter/material.dart';
import './transaction_item.dart';
import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTx;

  TransactionList(this.transactions, this.deleteTx);

  Widget build(BuildContext context) {
    return transactions.isEmpty
        ? LayoutBuilder(builder: (ctx, constraints) {
            return Column(
              children: [
                Text(
                  "No transactions added yet!",
                  style: Theme.of(context).textTheme.title,
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                    height: constraints.maxHeight * 0.6,
                    child: Image.asset("assets/images/waiting.png",
                        fit: BoxFit.cover))
              ],
            );
          })
        //
        //ListView.builder tem um bug(não é bug, ver doc) que nao
        //respeita a key passada.
        //ao apagar 1 item o state nao é apagado e fica com
        //referencias trocadas
        //
        //DOCUMENTATION
        //ListView.builder by default does not support child reordering.
        //If you are planning to change child order at a later time,
        //consider using ListView or ListView.custom.
        //
        : ListView.builder(
            itemBuilder: (ctx, index) {
              return TransactionItem(
                  key: Key(transactions[index].title),
                  transaction: transactions[index],
                  deleteTx: deleteTx);
            },
            itemCount: transactions.length,
          );
    //
    //OR
    //
    //USAR ASSIM APENAS QD TransactionItem for stateFull.
    //PARA ESTE CASO, FAZ MAIS SENTIDO USAR stateLess e voltar a
    //usar o ListView.builder
    //
    // : ListView(
    //     children: transactions
    //         .map((tx) => TransactionItem(
    //             key: ValueKey(tx.id), transaction: tx, deleteTx: deleteTx))
    //         .toList(),
    //   );
  }
}
